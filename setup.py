#! /usr/bin/env python

from setuptools import setup
from distutils.extension import Extension
try:
    from Cython.Build import cythonize
    import numpy as np
except ImportError:
    cythonize = lambda dummy: []


try:
    extensions = [
        Extension("jetcasc.synch", ["jetcasc/synch.pyx"],
                  include_dirs = [np.get_include()],
                  libraries = ['m'],
                  library_dirs = [],
                  extra_compile_args=['-std=c99', '-O3', '-Wall', '-Wextra']),
    ]
except NameError:   # when 'np' is not recognised
    extensions = []


setup(
    name="jetcasc",
    version="0.2",
    description="Jet cascade",
    author="Koushik Chatterjee, Evert Rol",
    author_email="K.Chatterjee@uva.nl",
    entry_points={
        'console_scripts': [
            'jetcalc=jetcasc.commands.jetcalc:main',
            'combine-outputs=jetcasc.commands.combine_outputs:main',
            'plot-sed=jetcasc.commands.plot_sed:main',
            ],
        },
    packages=["jetcasc", "jetcasc.commands"],
    package_dir={'jetcasc': "jetcasc"},
    ext_modules=cythonize(extensions),
    install_requires=["numpy", "scipy", "Cython", "h5py", "matplotlib"],
    data_files=[],
)
