from __future__ import division
import sys
import numpy as np
import h5py


with h5py.File(sys.argv[1]) as fp1, h5py.File(sys.argv[2]) as fp2:
    assert list(sorted(fp1.keys())) == list(sorted(fp2.keys()))
    for key in fp1.keys():
        ds1 = fp1[key][...]
        ds2 = fp2[key][...]
        print(key, ds1.dtype, ds2.dtype, ds1.shape, ds2.shape)
        m1 = np.mean(ds1)
        m2 = np.mean(ds2)
        assert np.allclose(m1/m2, 1)
        ds1 /= m1
        ds2 /= m2
        try:
            assert np.allclose(ds1, ds2, equal_nan=True)
        except AssertionError:
            diff = ds1 - ds2
            idx = np.where(np.abs(diff) > 1e-4)
            ds1 = fp1[key][0,i,...]
            ds2 = fp2[key][0,i,...]
            div = ds1/ds2
            idx = np.where(np.isnan(div))
            div[idx] = 1
            idx = np.where((div > 2) | (div < 0.5))
