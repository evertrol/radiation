import sys
import os
import argparse
import numpy as np
import h5py


parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    allow_abbrev=False
)
parser.add_argument('input', nargs='+',
                    help="Input file names")
parser.add_argument('-o', '--output', required=True,
                    help="Output file name")
parser.add_argument('-e', '--extend', action='store_true',
                    help="Extend output file, instead of overwriting")
args = parser.parse_args()

ftype = np.float32
for i, filename in enumerate(args.input):
    with open(filename) as fp:
        line = fp.readline()
        line_list = line.split()
        N1 = int(line_list[0])
        N2 = int(line_list[1])
        N3 = int(line_list[1])
        dx1 = ftype(line_list[2])
        dx2 = ftype(line_list[3])

    grmhd_data = np.loadtxt(filename, dtype=ftype, skiprows=1,
                            usecols=[2, 3, 4, 5, 6, 7], unpack=True, max_rows=N1*N2*N3)
    if len(grmhd_data[0]) == N1 * N2:  # Original 2D input
        N3 = 1
    dx3 = 2 * np.pi / N3

    grmhd = {}
    grmhd['r'], grmhd['h'], grmhd['rho'], grmhd['ug'], grmhd['bsq'], grmhd['gdet'] = grmhd_data
    for key, value in grmhd.items():
        grmhd[key] = value.reshape(1, N1, N2, -1)
    grmhd['gdet'] = grmhd['gdet'][...,0:1]

    print("Writing", filename, "to", args.output)
    if not i and not args.extend:
        with h5py.File(args.output, "w") as fp:
            for key, value in grmhd.items():
                maxshape = (None, value.shape[1], value.shape[2], value.shape[3])
                fp.create_dataset(key, data=value, maxshape=maxshape ,compression="gzip")
            fp.attrs['dx1'] = dx1
            fp.attrs['dx2'] = dx2
            fp.attrs['dx3'] = dx3
    else:
        with h5py.File(args.output) as fp:
            for key, value in grmhd.items():
                l = fp[key].shape[0]
                fp[key].resize(l+1, axis=0)
                fp[key][l,...] = value
