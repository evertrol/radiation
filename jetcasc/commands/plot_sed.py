import os
import argparse
import logging
import numpy as np
import h5py
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rc
from ..tools import setup_logger
from ..constants import PARSEC_CGS


logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        allow_abbrev=False
    )
    parser.add_argument('input', help="Analyses HDF 5 output file")
    parser.add_argument('-o', '--output',
                        help="Plot file name")
    parser.add_argument('-f', '--frequency', nargs=3, type=float,
                        default=[8, 20, 120],
                        help="Frequency distribution: min, max, n")
    parser.add_argument('-t', '--timestep', default=0, type=int,
                        help="Specify time step")
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help="Verbosity level")
    args = parser.parse_args()
    if not args.output:
        args.output = os.path.splitext(args.input)[0] + ".png"
    return args


def readdata(filename, timestep=0):
    t = timestep
    with h5py.File(filename, "r") as fp:
        # Sum over the spatial dimensions when needed
        # First axis is time, last axis frequency
        if fp['ssc'].ndim == 2:
            ssc = fp['ssc'][t,...]
        else:
            ssc = fp['ssc'][t,...].sum(axis=-2).sum(axis=-2).sum(axis=-2)
        if fp['mj'].ndim == 2:
            mj = fp['mj'][t,...]
        else:
            mj = fp['mj'][t,...].sum(axis=-2).sum(axis=-2).sum(axis=-2)
    return ssc, mj


def setup_mpl():
    # Add amsmath to the preamble
    mpl.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
    rc('text', usetex=True)
    font = {'size': 20}
    rc('font', **font)
    rc('xtick', labelsize=20)
    rc('ytick', labelsize=20)
    mpl.rcParams['font.family'] = 'serif'
    mpl.rcParams['font.serif'] = 'cmr10'
    mpl.rcParams['font.sans-serif'] = 'cmr10'
    mpl.rcParams['axes.unicode_minus'] = False
    legend = {'fontsize': 20}
    rc('legend', **legend)
    axes = {'labelsize': 20}
    rc('axes', **axes)


def plot(filename, ssc, mj, freq):
    scaling = 4 * np.pi * (7860 * PARSEC_CGS)**2
    figure = plt.figure(figsize=(8, 8))
    plt.plot(freq, freq * mj * scaling, color="k", linewidth=3)
    plt.plot(freq, freq * ssc * scaling, color="blue", linewidth=3)
    plt.xscale("log")
    plt.yscale("log")
    plt.ylim(1e30, 2e36)
    plt.xlim(1e8, 1e20)
    plt.xlabel(r"$\nu$ (Hz)", fontsize=30)
    plt.ylabel(r"$\nu L_{\nu}$ (erg s$^{-1}$)", fontsize=30)
    figure.savefig(filename)


def run(input, output, timestep=0, frequency=(8, 20, 120)):
    ssc, mj = readdata(input, timestep=timestep)
    freq = np.logspace(frequency[0], frequency[1], frequency[2],
                       endpoint=False)
    if len(freq) != len(ssc) != len(mj):
        raise ValueError("Frequency, SSC and MJ array lengths don't match: "
                         "{}, {} and {}".format(len(freq), len(ssc), len(mj)))

    plot(output, ssc, mj, freq)


def main():
    args = parse_args()
    setup_logger(verbose=args.verbose)
    run(args.input,
        args.output,
        timestep=args.timestep,
        frequency=args.frequency)


if __name__ == '__main__':
    main()
