import argparse
import logging
import numpy as np
import h5py
from jetcasc import calc
from ..tools import setup_logger


logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        allow_abbrev=False
    )
    parser.add_argument('input', help="Input file")
    parser.add_argument('output', help="Output file")
    parser.add_argument('-g', '--lorentz', nargs=3, type=float,
                        default=[0.1, 6.1, 60],
                        help="Lorentz factor distribution: min, max, n")
    parser.add_argument('-f', '--frequency', nargs=3, type=float,
                        default=[8, 20, 120],
                        help="Frequency distribution: min, max, n")
    parser.add_argument('-F', '--frequency_ssc', nargs=3, type=float,
                        default=[8, 20, 120],
                        help="SSC frequency distribution: min, max, n")
    parser.add_argument('-i', '--islice', nargs=2, type=int,
                        help="Limit outside loop/variable (start, end)")
    parser.add_argument('-j', '--jslice', nargs=2, type=int,
                        help="Limit intermediate loop/variable (start, end)")
    parser.add_argument('-k', '--kslice', nargs=2, type=int,
                        help="Limit inner loop/variable (start, end)")
    parser.add_argument('-t', '--timeslice', nargs='+', type=int,
                        help="Limit time loop/variable (start, end, [step])")
    parser.add_argument('-C', '--compression', choices=[None, 'gzip'],
                        help="Compression type for output")
    parser.add_argument('-n', '--nproc', type=int, default=1,
                        help="Number of processes / cores")
    parser.add_argument('-c', '--chunksize', type=int, default=1,
                        help="Chunksize for multiprocessing")
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help="Verbosity level")
    args = parser.parse_args()
    args.lorentz[2] = int(args.lorentz[2])
    args.frequency[2] = int(args.frequency[2])
    args.frequency_ssc[2] = int(args.frequency_ssc[2])
    if args.timeslice and len(args.timeslice) not in [2, 3]:
        raise ValueError('timeslice should be 0, 2 or 3 arguments')
    return args


def read_data(filename):
    grmhd = {}
    with h5py.File(filename, "r") as fp:
        dx1 = fp.attrs['dx1']
        dx2 = fp.attrs['dx2']
        dx3 = fp.attrs.get('dx3', 2 * np.pi)
        for key in fp.keys():
            grmhd[key] = fp[key][:]
    return grmhd, (dx1, dx2, dx3)


def write_data(filename, grmhd, ssc, mj, comp=None, slices=None):
    with h5py.File(filename, "w") as fp:
        for key, value in grmhd.items():
            fp.create_dataset(key, data=value, compression=comp, shuffle=True, fletcher32=True)
        fp.create_dataset("ssc", data=ssc, compression=comp, shuffle=True, fletcher32=True)
        fp.create_dataset("mj", data=mj, compression=comp, shuffle=True, fletcher32=True)

        if slices:
            if slices[0]:
                if len(slices[0]) == 2:
                    slices[0].append(1)
                fp.attrs['timeslice'] = slices[0]
            for i, key in zip([1, 2, 3], ['i', 'j', 'k']):
                if slices[i]:
                    fp.attrs[key + 'slice'] = slices[i]


def run(input,
        output,
        islice=None,
        jslice=None,
        kslice=None,
        timeslice=None,
        lorentz=(0.1, 6.1, 60),
        frequency=(8, 20, 120),
        frequency_ssc=(8, 20, 120),
        compression=None,
        nproc=1,
        chunksize=1):

    dist_g = np.linspace(*lorentz, endpoint=False)
    dist_f = np.linspace(*frequency, endpoint=False)
    dist_fssc = np.linspace(*frequency_ssc, endpoint=False)

    grmhd, dx = read_data(input)

    slices = timeslice, islice, jslice, kslice
    nt, ni, nj, nk = slices
    ssc, mj = calc(grmhd=grmhd, dx=dx, dist_g=dist_g, dist_f=dist_f, dist_fssc=dist_fssc,
                   nt=nt, ni=ni, nj=nj, nk=nk, nproc=nproc, chunksize=chunksize)

    write_data(output, grmhd, ssc, mj, comp=compression, slices=slices)


def main():
    args = parse_args()
    setup_logger(verbose=args.verbose)
    run(args.input,
        args.output,
        islice=args.islice,
        jslice=args.jslice,
        kslice=args.kslice,
        timeslice=args.timeslice,
        lorentz=args.lorentz,
        frequency=args.frequency,
        frequency_ssc=args.frequency_ssc,
        compression=args.compression,
        nproc=args.nproc,
        chunksize=args.chunksize)


if __name__ == '__main__':
    main()
