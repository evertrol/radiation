import os
import argparse
import logging
from collections import defaultdict
import numpy as np
import h5py
from jetcasc.tools import setup_logger


logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        allow_abbrev=False
    )
    parser.add_argument('input', nargs='+', help="Input files to combine")
    parser.add_argument('-o', '--output',
                        help="Output combined file")
    parser.add_argument('-C', '--compression', choices=[None, 'gzip'], default=None,
                        help="Compression type")
    parser.add_argument('-c', '--chunksize', default=None, type=int, nargs='+',
                        help="Set chunksize manually. Set to 0 to turn off")
    parser.add_argument('-I', '--no-input-data', action='store_true',
                        help="Do not store the input data next to the output variables")
    parser.add_argument('--sum', action='store_true',
                        help="Store output variables already summed "
                        "over the spatial dimensions")
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help="Verbosity level")
    args = parser.parse_args()
    if not args.output:
        args.output = os.path.commonprefix(args.input)
        if not args.output:
            raise ValueError("Could not determine an output file name. "
                             "Please provide one.")
        args.output += ".h5"
    return args


def write_data(output, input, dims, compression=None, chunks=None,
               no_input_data=False, sum=False):
    n = dims
    with h5py.File(output, "w") as fout:
        if not no_input_data:
            for key in ['r', 'h', 'rho', 'ug', 'bsq']:
                fout.create_dataset(key, dtype=np.float64,
                                    shape=(n['t'], n['i'], n['j'], n['k']),
                                    compression=compression, chunks=chunks,
                                    shuffle=True, fletcher32=True)
            fout.create_dataset('gdet', dtype=np.float64,
                                shape=(n['t'], n['i'], n['j'], 1),
                                compression=compression, shuffle=True, fletcher32=True)
        if sum:
            fout.create_dataset('mj', dtype=np.float64, shape=(n['t'], n['f']),
                                compression=compression, shuffle=True, fletcher32=True)
            fout.create_dataset('ssc', dtype=np.float64, shape=(n['t'], n['f']),
                                compression=compression, shuffle=True, fletcher32=True)
        else:
            if chunks:
                chunks = chunks + (n['f'],)
            fout.create_dataset('mj', dtype=np.float64,
                                shape=(n['t'], n['i'], n['j'], n['k'], n['f']),
                                compression=compression, chunks=chunks,
                                shuffle=True, fletcher32=True)
            fout.create_dataset('ssc', dtype=np.float64,
                                shape=(n['t'], n['i'], n['j'], n['k'], n['f']),
                                compression=compression, chunks=chunks,
                                shuffle=True, fletcher32=True)

        for filename in input:
            with h5py.File(filename, "r") as fp:
                logger.info("Adding file %s", filename)
                s = {}
                for key in ['time', 'i', 'j', 'k']:
                    s[key] = slice(*fp.attrs.get(key + 'slice', [None]))
                s['t'] = s['time']
                if not no_input_data:
                    for key in ['r', 'h', 'rho', 'ug', 'bsq']:
                        fout[key][s['t'], s['i'], s['j'], s['k']] = fp[key]
                    fout['gdet'][s['t'], s['i'], s['j'], s['k']] = fp['gdet']
                for key in ['ssc', 'mj']:
                    if sum:
                        # Sum over the spatial dimensions
                        total = fp[key][:].sum(axis=-2).sum(axis=-2).sum(axis=-2)
                        fout[key][s['t'],:] += total
                    else:
                        fout[key][s['t'], s['i'], s['j'], s['k'], :] = fp[key]


def run(input, output,
        compression=None, chunksize=None, no_input_data=False, sum=False):
    slices = defaultdict(list)
    for filename in input:
        with h5py.File(filename, "r") as fp:
            for key in ['time', 'i', 'j', 'k']:
                value = fp.attrs.get(key + 'slice')
                if value is not None:
                    slices[key].append(value)
            shape = fp['ssc'].shape
    dims = {}
    dims['t'], dims['i'], dims['j'], dims['k'], dims['f'] = shape
    if 'time' in slices:
        start, stop, step = slices['time'][0]
        for s in slices['time']:
            if s[0] < start:
                start = s[0]
            if s[1] > stop:
                stop = s[1]
        dims['t'] = (stop-start) // step
    for key in ['i', 'j', 'k']:
        if key in slices:
            start, stop = slices[key][0]
            for s in slices[key]:
                if s[0] < start:
                    start = s[0]
                if s[1] > stop:
                    stop = s[1]
            dims[key] = stop - start

    logger.debug("Input dimensions = %s", dims)
    if chunksize is None:
        chunks = None
        if 'k' not in slices:
            if 'j' not in slices:
                chunks = (1, 1) + shape[-3:-1]
            else:
                chunks = (1, 1, 1) + (shape[-2],)
        elif 'j' not in slices:
            chunks = (1, 1) + (shape[-3], 1)
    elif chunksize == [0]:
        chunks = None
    else:
        chunks = tuple(chunksize)
    logger.debug('chunk size = %s', chunks)

    write_data(output, input, dims, compression=compression, chunks=chunks,
               no_input_data=no_input_data, sum=sum)


def main():
    args = parse_args()
    setup_logger(verbose=args.verbose)
    run(args.input,
        args.output,
        compression=args.compression,
        chunksize=args.chunksize,
        no_input_data=args.no_input_data,
        sum=args.sum)


if __name__ == '__main__':
    main()
