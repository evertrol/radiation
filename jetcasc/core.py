from __future__ import division
import logging
from contextlib import closing   # Python 2 compatibility
from functools import partial
import multiprocessing as mp
import numpy as np
import scipy.special as scsp
from .synch import ssc_trial, mj_trial
from .constants import *
from .tools import TRACE


logger = logging.getLogger(__name__)


F_T = 0.3    # Temperature ratio (F_T=T_e/T_p)

# CGS constants : Sagittarius A*
# Scaling from code units to cgs units
M_SGRA_SOLAR = 4.6e6          # Solar masses
R_G_CGS = M_SGRA_SOLAR * M_SOLAR_CGS * G_CGS / (C_CGS * C_CGS)  # Gravitational radius
R_GOC_CGS = R_G_CGS / C_CGS                                     # Light-crossing time
MASS_DENSITY_SCALE = 6e-16
ENERGY_DENSITY_SCALE = MASS_DENSITY_SCALE * C_CGS * C_CGS
MAGNETIC_DENSITY_SCALE = np.sqrt(MASS_DENSITY_SCALE) * C_CGS
PRESSURE_SCALE = MASS_DENSITY_SCALE * C_CGS * C_CGS
X_NU_STAR = 8.093307707472243e-21


def calc_inner(i, t, grmhd, dv, temp_p, b_abs, density,
               dist_g, dist_f, dist_fssc, jrange, krange):
    offsetj = jrange.start
    nj = jrange.stop - jrange.start
    offsetk = krange.start
    nk = krange.stop - krange.start
    nf = len(dist_f)

    mj = np.zeros((nj, nk, nf))
    ssc = np.zeros((nj, nk, nf))

    bsq_rho_ratio = (grmhd['bsq'] / grmhd['rho'])[t,i,...]
    gdet = grmhd['gdet'][t,i,...,0]
    b_abs = b_abs[t,i,...]
    density = density[t,i,...]
    temp_p = temp_p[t,i,...]
    gdist = 10**dist_g

    for j in jrange:
        for k in krange:
            flag_ssa = 0   # Frequency index of synchrotron self absorption

            # t_e is the dimensionless temperature
            temp_e_cgs = F_T * temp_p[j,k]
            temp_e = BOLTZ_CGS * temp_e_cgs / (ERM_CGS * C_CGS**2)

            # Only include low magnetized region with high enough magnetix field
            logger.log(TRACE, "bsq, rho, ratio, b_abs = %f, %f, %f, %f",
                       grmhd['bsq'][t,i,j,k], grmhd['rho'][t,i,j,k],
                       bsq_rho_ratio[j,k], b_abs[j,k])
            if bsq_rho_ratio[j,k] < 1 and b_abs[j,k] > 1e-4:
                b_field = b_abs[j,k]
                n_tot = density[j,k] / MH_CGS
                radius = ((3/(4 * PI) * gdet[j] * dv * R_G_CGS**3)**(1/3))
                # Electron distribution in Lorentz factor: Maxwell-Juttner
                div = temp_e * scsp.kv(2, 1 / temp_e)
                mj_n = n_tot * (gdist**2 * (1 - gdist**-2)**0.5 * np.exp(-gdist / temp_e)) / div

                # Synchrotron emission
                #mj_init, flag_ssa = mj_trial(mj_n, b_field, radius, 1)
                mj_init, flag_ssa = mj_trial_py(mj_n, b_field, radius, doppler=1,
                                                dist_g=dist_g, dist_f=dist_f)

                # SSC emission
                #ssc_init = ssc_trial_py(mj_n, mj_init, radius, 1, dist_g, dist_f, dist_fssc)
                ssc_init = ssc_trial(mj_n, mj_init, radius, 1, dist_g, dist_f, dist_fssc)

                # Synchrotron and SSC fluxes
                area = PI * radius**2 / ((7860 * PARSEC_CGS)**2)
                mj[j-offsetj,k-offsetk,:flag_ssa] = mj_init[:flag_ssa] * area
                mj[j-offsetj,k-offsetk,flag_ssa:] = mj_init[flag_ssa:] * 4/3 * area
                if n_tot * THOMSON_CGS * radius > 1:
                    ssc[j-offsetj,k-offsetk,:] = ssc_init * area
                else:
                    ssc[j-offsetj,k-offsetk,:] = ssc_init * 4/3 * area
    logger.debug("step %d, r = %f", i, grmhd['r'][t,i,0,0])
    return ssc, mj


def calc(grmhd, dx, dist_g, dist_f, dist_fssc, nt=None, ni=None, nj=None, nk=None,
         nproc=None, chunksize=1):
    dx1, dx2, dx3 = dx
    dv = dx1 * dx2  * dx3
    b_abs = np.sqrt(grmhd['bsq'] * 4 * PI) * MAGNETIC_DENSITY_SCALE
    density = grmhd['rho'] * MASS_DENSITY_SCALE
    internal_energy = grmhd['ug'] * ENERGY_DENSITY_SCALE
    temp_p = MMW * MH_CGS * (4/3 - 1) * internal_energy / (BOLTZ_CGS * density)

    _nt, _ni, _nj, _nk = grmhd['r'].shape

    if nt is None:
        nt = [0, _nt]
    if len(nt) == 2:
        nt.append(1)
    tslice = slice(*nt)
    trange = list(range(_nt)[tslice])
    nt = (nt[1] - nt[0]) // nt[2]

    if ni is None:
        ni = [0, _ni]
    islice = slice(*ni)
    irange = list(range(_ni)[islice])
    ioffset = ni[0]
    ni = ni[1] - ni[0]

    if nj is None:
        nj = [0, _nj]
    jslice = slice(*nj)
    jrange = range(_nj)[jslice]
    nj = jslice.stop - jslice.start

    if nk is None:
        nk = [0, _nk]
    kslice = slice(*nk)
    krange = range(_nk)[kslice]
    nk = kslice.stop - kslice.start
    nf = len(dist_f)

    mj = np.zeros((nt, ni, nj, nk, nf))
    ssc = np.zeros((nt, ni, nj, nk, nf))

    for it, t in enumerate(trange):
        logger.debug("Timestamp %d", t)
        func = partial(calc_inner, t=t, grmhd=grmhd, dv=dv, temp_p=temp_p,
                       b_abs=b_abs, density=density,
                       dist_g=dist_g, dist_f=dist_f, dist_fssc=dist_fssc,
                       jrange=jrange, krange=krange)
        if nproc > 1:
            with closing(mp.Pool(processes=nproc)) as pool:
                results = pool.imap(func, irange, chunksize=chunksize)
                for i, result in enumerate(results):
                    ssc[it,i-ioffset,0:nj,0:nk,:] = result[0]
                    mj[it,i-ioffset,0:nj,0:nk,:] = result[1]
        else:
            for i in irange:
                result = func(i)
                ssc[it,i-ioffset,0:nj,0:nk,:] = result[0]
                mj[it,i-ioffset,0:nj,0:nk,:] = result[1]

    # reduce the input dimensions as well
    for key in grmhd.keys():
        if key == 'gdet':
            grmhd[key] = grmhd[key][tslice, islice, jslice, ...]
        else:
            grmhd[key] = grmhd[key][tslice, islice, jslice, kslice]


    return ssc, mj


def mj_trial_py(dist_n, B_field, Rvol, doppler, dist_g, dist_f):
    gstep = np.diff(dist_g)
    gstep = np.hstack((gstep, gstep[-1]))
    fstep = np.diff(dist_f)
    fstep = np.hstack((fstep, fstep[-1]))

    dist_f2 = dist_f - np.log10(doppler)

    nu_B = E_CGS * B_field / (2. * PI * ERM_CGS * C_CGS)

    t_cg = np.power(10, dist_f2[...,None]) / (3 * (np.power(10, (2 * dist_g[None,...]))) * nu_B)
    kv4_3 = scsp.kv(4/3, t_cg)
    kv1_3 = scsp.kv(1/3, t_cg)
    k_factor = (kv4_3 * kv1_3 - (3/5) * t_cg * (np.power(kv4_3, 2) - np.power(kv1_3, 2)))

    dist_p = ((5.196152423 * THOMSON_CGS * C_CGS * B_field**2 * t_cg**2) /
              (8.0 * PI * PI * nu_B)) * k_factor

    dist_j2 = ((dist_n[None,...] * dist_p * np.power(10, dist_g[None,...]) * LN10 * gstep) /
               (4 * PI)).sum(axis=-1)
    pow10g = 10**dist_g
    x = pow10g * np.sqrt(pow10g**2 - 1)
    x2 = x * dist_p
    g_factor = (x2[...,2:] - x2[...,:-2]) * ERM_CGS * C_CGS / (pow10g[2:] - pow10g[:-2])
    k_alpha = ((1 / (8 * PI * ERM_CGS * np.power(10, (2 * dist_f2[...,None])))) *
               (dist_n[None,1:-1] / (x[None,1:-1] * ERM_CGS * C_CGS)) *
               g_factor * pow10g[None,1:-1] * LN10 * gstep[1:-1]).sum(axis=-1)

    boundary = k_alpha * Rvol
    low = boundary < 1e-3
    high = boundary > 1e3
    middle = ~low & ~high
    dist_i = np.zeros(dist_j2.shape)
    dist_i[middle] = ((dist_j2[middle] / k_alpha[middle]) *
                      (1 - np.exp(-k_alpha[middle] * Rvol)))
    dist_i[low] = dist_j2[low] * Rvol
    dist_i[high] = dist_j2[high] / k_alpha[high]

    flag = (boundary > 1) & (~high)
    try:
        iflag = np.where(flag)[0][-1]
    except IndexError:
        iflag = 0

    return dist_i, iflag


def ssc_trial_py(dist_n, dist_is, Rvol, doppler,
                 dist_g, dist_f, dist_fssc):
    fstep = np.diff(dist_f)
    fstep = np.hstack((fstep, fstep[-1]))
    gstep = np.diff(dist_g)
    gstep = np.hstack((gstep, gstep[-1]))
    dist_g = 10**dist_g
    beta = np.sqrt(1 - 1 / (dist_g**2))
    beta_dem = 1 / (dist_g**2 - 1)
    beta_k1 = (1 - beta) / (1 + beta)
    beta_k2 = (1 + beta) / (1 - beta)

    n = len(dist_is)
    dist_f2 = 10**dist_f / doppler
    x_nu = X_NU_STAR * dist_f2
    dist_fssc2 = dist_fssc - np.log10(doppler)
    dist_issc = np.zeros(n)
    ones = np.ones(n)

    f_ij = np.power(10, dist_fssc2[...,None] - dist_fssc2[None,...])
    gamma1 = np.maximum(ones, np.sqrt(0.25 * f_ij))
    gamma2 = np.minimum(1e6*ones, 3 * ERM_CGS * C_CGS**2 / (4. * PLANCK_CGS * dist_f2))
    flag = ((dist_g[None,None,...] * x_nu[None,...,None] < 0.75) &
            (dist_g[None,None,...] > gamma1[...,None]) &
            (dist_g[None,None,...] < gamma2[...,None]))

    g1 = flag & (f_ij[...,None] <= 1) & (f_ij[...,None] >= beta_k1[None,None,...])
    g2 = flag & (f_ij[...,None] >= 1) & (f_ij[...,None] <= beta_k2[None,None,...])
    f = THOMSON_CGS * dist_g * LN10 * dist_n * (1 + beta) * beta_dem * gstep

    m1 = f[None,None,...] * (f_ij[...,None] - beta_k1[None,...])
    m2 = f[None,None,...] * (1 - f_ij[...,None] * beta_k1[None,...])

    int1 = np.where(g1, m1, 0).sum(axis=-1) + np.where(g2, m2, 0).sum(axis=-1)

    int2 = (LN10 * fstep * int1 * dist_is[None,...] / dist_f2[None,...]).sum(axis=-1)
    dist_issc = int2 * dist_f2 * 0.25 * Rvol

    return dist_issc
