#ifndef SSC_H
#define SSC_H

#define LN10 2.302585093
#define THOMSON_CGS 6.652e-25
#define ERM_CGS 9.10938215e-28
#define E_CGS 4.80320427e-10
#define C_CGS 2.99792458e10
#define PARSEC_CGS 3.086e18
#define PLANCK_CGS 6.6260755e-27
#define X_NU_STAR 8.093307707472243e-21   //  = PLANCK_CGS / (ERM_CGS * C_CGS * C_CGS);

#include <stdlib.h>   // for size_t

int
ssc(const double *dist_N, const double *dist_Is, double *dist_Issc,
	size_t n_dist_N, size_t n_dist_Is, size_t n_dist_Issc,
	double Rvol, double doppler,
	double *gdist, double *fdist, double *fsscdist,
	size_t ng, size_t nf, size_t nfssc);

#endif // SSC_H
