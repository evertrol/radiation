#include "ssc.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>


int
ssc(const double *dist_N, const double *dist_Is, double *dist_Issc,
	size_t n_dist_N, size_t n_dist_Is, size_t n_dist_Issc,
	double Rvol, double doppler,
	double *gdist, double *fdist, double *fsscdist,
	size_t ng, size_t nf, size_t nfssc)
{
		double *dist_G = NULL;
		double *beta = NULL;
		double *beta_dem = NULL;
		double *beta_k1 = NULL;
		double *beta_k2 = NULL;
		double *dist_F2 = NULL;
		double *x_nu = NULL;
		double *dist_Fssc2 = NULL;
		double int1 = 0.0;
		double int2 = 0.0;
		double F_ij = 0.0;
		double *gstep = NULL;
		double *fstep = NULL;
		double gamma1 = 0.0;
		double gamma2 = 0.0;
		bool flagG1 = 0.0;
		bool flagG2 = 0.0;

		dist_G = malloc(n_dist_N * sizeof *dist_G);
		beta = malloc(n_dist_N * sizeof *beta);
		beta_dem = malloc(n_dist_N * sizeof *beta_dem);
		beta_k1 = malloc(n_dist_N * sizeof *beta_k1);
		beta_k2 = malloc(n_dist_N * sizeof *beta_k2);
		dist_F2 = malloc(n_dist_Is * sizeof *dist_F2);
		x_nu = malloc(n_dist_Is * sizeof *x_nu);
		dist_Fssc2 = malloc(n_dist_Is * sizeof *dist_Fssc2);
		gstep = malloc(ng * sizeof *gstep);
		fstep = malloc(nf * sizeof *fstep);
		if (dist_G == NULL || beta == NULL  || beta_dem == NULL  ||
			beta_k1 == NULL || beta_k2 == NULL || dist_F2 == NULL ||
			x_nu == NULL || dist_Fssc2 == NULL ||
				gstep == NULL || fstep == NULL) {
				free(dist_G);
				free(beta);
				free(beta_dem);
				free(beta_k1);
				free(beta_k2);
				free(dist_F2);
				free(x_nu);
				free(dist_Fssc2);
				free(gstep);
				free(fstep);
				return 1;
		}

		// assert gets stripped by the default Python extension
		// compilation (-DNDEBUG option), so we check things
		// ourselves.
		if (n_dist_N != ng) {
				fprintf(stderr, "n_dist_N != ng: %zd != %zd", n_dist_N, ng);
				abort();
		}
		if (n_dist_Is != nf) {
				fprintf(stderr, "n_dist_Is != nf: %zd != %zd", n_dist_Is, nf);
				abort();
		}
		if (n_dist_Issc != nfssc) {
				fprintf(stderr, "n_dist_Issc != nfssc: %zd != %zd", n_dist_Issc, nfssc);
				abort();
		}

		for (size_t i = 0; i < n_dist_N; i++) {
				dist_G[i] = pow(10., gdist[i]);
				beta[i] = sqrt(1. - 1. / (dist_G[i] * dist_G[i]));
				beta_dem[i] = 1.0 / (dist_G[i] * dist_G[i] - 1.0);
				beta_k1[i] = (1.0 - beta[i]) / (1.0 + beta[i]);
				beta_k2[i] = (1.0 + beta[i]) / (1.0 - beta[i]);
		}

		for (size_t i = 0; i < n_dist_Is; i++) {
				dist_F2[i] = pow(10.0, fdist[i]) / doppler;
				x_nu[i] = X_NU_STAR * dist_F2[i];
				dist_Fssc2[i] = fsscdist[i] - log10(doppler);
				dist_Issc[i] = 0.0;
		}

		for (size_t i = 0; i < ng-1; i++) {
				gstep[i] = gdist[i+1] - gdist[i];
		}
		if (ng > 1) {
				gstep[ng-1] = gstep[ng-2];
		} else if (ng > 0) {
				gstep[0] = 0;
		}
		for (size_t i = 0; i < nf-1; i++) {
				fstep[i] = fdist[i+1] - fdist[i];
		}
		if (nf > 1) {
				fstep[nf-1] = fstep[nf-2];
		} else if (nf > 0) {
				fstep[0] = 0;
		}

		for (size_t i = 0; i < n_dist_Issc; i++) {
				int2 = 0.0;
				for (size_t j = 0; j < n_dist_Is; j++) {
						int1 = 0.0;
						F_ij = pow(10., dist_Fssc2[i] - dist_Fssc2[j]);
						gamma1 = fmax(1.0, sqrt(0.25 * ((F_ij))));
						gamma2 = fmin(1.0e6, 3. * ERM_CGS * C_CGS * C_CGS /
									  (4. * PLANCK_CGS * (dist_F2[j])));
						for (size_t k = 0; k < n_dist_N; k++) {
								bool flag = ((dist_G[k] * x_nu[j] < 0.75) &
											 (dist_G[k] > gamma1) &
											 (dist_G[k] < gamma2));
								if (!flag) {
										continue;
								}
								flagG1 = (flag &
										  (F_ij <= 1.0) &
										  (F_ij >= beta_k1[k]));
								flagG2 = (flag &
										  (F_ij >= 1.0) &
										  (F_ij <= beta_k2[k]));
								if (flagG1 || flagG2) {
										int1 += (THOMSON_CGS * dist_G[k] * LN10 *
												 gstep[k] * dist_N[k] * (1. + beta[k]) * beta_dem[k] *
												 (flagG1 * (F_ij - beta_k1[k]) +
												  flagG2 * (1.0 - ((F_ij)) * beta_k1[k])));
								}
						}
						int2 += LN10 * fstep[j] * int1 * dist_Is[j] / dist_F2[j];
				}
				dist_Issc[i] = int2 * dist_F2[i] * 0.25 * Rvol;
		}

		free(dist_G);
		free(beta);
		free(beta_dem);
		free(beta_k1);
		free(beta_k2);
		free(dist_F2);
		free(x_nu);
		free(dist_Fssc2);
		free(fstep);
		free(gstep);

		return 0;
}
