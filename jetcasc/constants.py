import numpy as np


PI = np.pi
LN10 = np.log(10)
MH_CGS = 1.673534e-24         # Mass hydrogen molecule
MMW = 1.69                    # Mean molecular weight
BOLTZ_CGS = 1.3806504e-16     # Boltzmanns constant
THOMSON_CGS = 6.652e-25       # Thomson cross section
PLANCK_CGS = 6.6260755e-27    # Planck's constant
ERM_CGS = 9.10938215e-28      # Electron rest mass
E_CGS = 4.80320427e-10        # Elementary charge
C_CGS = 2.99792458e10         # Speed of light
M_SOLAR_CGS = 1.998e33        # Solar mass
G_CGS = 6.67259e-8            # Gravitational constant
PARSEC_CGS = 3.086e18         # CGS
