import logging


TRACE = 5
logging.addLevelName(TRACE, "TRACE")


def setup_logger(level=logging.WARN, name=None, verbose=None):
    if verbose is not None:
        level = [logging.WARN, logging.INFO, logging.DEBUG, TRACE][verbose]
    if name is not None:
        name = __name__.split('.')[0]

    logger = logging.getLogger('jetcasc')
    logger.setLevel(level)
    handler = logging.StreamHandler()
    handler.setLevel(level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                  datefmt="%Y-%m-%dT%H:%M:%S")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger
