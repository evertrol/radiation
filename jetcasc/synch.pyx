# cython: language_level=3
import sys
import scipy
import scipy.special as scsp
import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport sqrt, pow, log10

@cython.wraparound(False)
@cython.boundscheck(False)
@cython.cdivision(True)


cpdef mj_trial(double[::1] dist_N, double B_field, double Rvol, double doppler):
    cdef double  THOMSON_CGS = 6.652e-25
    cdef double PI = np.pi
    cdef double  ERM_CGS = 9.10938215e-28
    cdef double  E_CGS = 4.80320427e-10
    cdef double  C_CGS = 2.99792458e10
    cdef double  PARSEC_CGS = 3.086e18
    cdef int k1, k2, i, j
    cdef double t_CG = 0.0
    cdef double nu_B = 0.0
    cdef double g_factor = 0.0
    cdef double K_factor = 0.0
    cdef double dist_G[60]
    cdef double dist_F2[120]
    cdef double dist_P[7200]
    cdef double k_alpha[120]
    cdef double dist_I[120]
    cdef double dist_J2[120]
    cdef int flag_ssa = 0

    for k1 in range(60):
        dist_G[k1] = (0.1 + 0.1*k1)
    for k1 in range(120):
        dist_F2[k1] = (8.0 + 0.1*k1) - log10(doppler)
        k_alpha[k1] = 0.0
        dist_I[k1] = 0.0
        dist_J2[k1] = 0.0
    for k1 in range(120):
        for k2 in range(60):
            dist_P[k1*60+k2] = 0.0
    nu_B = E_CGS * B_field / (2. * PI * ERM_CGS * C_CGS)

    for k1 in range(120):
        for k2 in range(60):
            t_CG = pow(10.0, dist_F2[k1]) / (3.0 * (pow(10.0, (2.0 * dist_G[k2]))) * nu_B)
            K_factor = (scsp.kv(4.0 / 3.0, t_CG) * scsp.kv(1.0 / 3.0, t_CG) -
                        (3.0 / 5.0) * t_CG * (pow((scsp.kv(4.0 / 3.0, t_CG)), 2.0) -
                                              pow(scsp.kv(1.0 / 3.0, t_CG), 2.0)))
            dist_P[k1*60+k2] = ((5.196152423 * THOMSON_CGS * C_CGS * B_field**2 * t_CG**2) /
                                (8.0 * PI * PI * nu_B)) * K_factor

    for k1 in range(120):
        for k2 in range(60):
            dist_J2[k1] += (dist_N[k2] * dist_P[k1*60+k2] *
                            pow(10.0, dist_G[k2]) * 2.302585093 * 0.1) / (4.0 * PI)

    for i in range(120):
        for j in range(1, 59):
            g_factor = ((((
                pow(10.0, (1.0 * dist_G[j+1])) * sqrt(pow(10.0, (2.0 * dist_G[j+1])) - 1.0)) *
                          dist_P[i*60+j+1]) - ((pow(10.0, (1.0 * dist_G[j-1])) *
                                                sqrt(pow(10.0, (2.0 * dist_G[j-1])) - 1.0)) *
                                               dist_P[i*60+j-1])) *
                        ERM_CGS * C_CGS / (pow(10.0, (dist_G[j+1])) - pow(10.0, (dist_G[j-1]))))
            k_alpha[i] += ((1.0 / (8.0 * PI * ERM_CGS * pow(10.0, (2.0 * dist_F2[i])))) *
                           (dist_N[j] / (pow(10.0, (1.0 * dist_G[j])) *
                                         sqrt(pow(10.0, (2.0 * dist_G[j])) - 1.0) * ERM_CGS * C_CGS)) *
                           g_factor * pow(10., dist_G[j]) * 2.302585093 * 0.1)

    for i in range(120):
        if k_alpha[i] * Rvol < 0.001:
            dist_I[i] = dist_J2[i] * Rvol
        elif k_alpha[i] * Rvol > 1000.0:
            dist_I[i] = dist_J2[i] / (k_alpha[i])
        else:
            dist_I[i] = (dist_J2[i] / k_alpha[i]) * (1. - pow(2.7182818285, -k_alpha[i] * Rvol))
            if k_alpha[i] * Rvol > 1.0:
                flag_ssa = i

    return (np.asarray(dist_I), flag_ssa)


cdef extern from "ssc.c":
    int ssc(const double *dist_N, const double *dist_Is, double *dist_Issc,
            int n_dist_N, int n_dist_Is, int n_dist_Issc,
            double Rvol, double doppler,
	    double *gdist, double *fdist, double *fsscdist,
	    size_t ng, size_t nf, size_t nfssc);

def ssc_trial(
        np.ndarray[double, ndim=1, mode="c"] dist_N not None,
        np.ndarray[double, ndim=1, mode="c"] dist_Is not None,
        double Rvol,
        double doppler,
        np.ndarray[double, ndim=1, mode="c"] dist_g not None,
        np.ndarray[double, ndim=1, mode="c"] dist_f not None,
        np.ndarray[double, ndim=1, mode="c"] dist_fssc not None):

    dist_Issc = np.zeros(len(dist_Is), dtype=np.double)
    cdef double[:] dist_Issc_view = dist_Issc

    result = ssc(&dist_N[0], &dist_Is[0], &dist_Issc_view[0],
                 len(dist_N), len(dist_Is), len(dist_Issc),
                 Rvol, doppler, &dist_g[0], &dist_f[0], &dist_fssc[0],
                 len(dist_g), len(dist_f), len(dist_fssc));
    if result:
        print("Error in ssc(). Exiting", file=sys.stderr)
        sys.exit(result)

    return dist_Issc
